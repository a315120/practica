const Sequelize = require('sequelize');


const actorModel = require('./models/actor');
const bookingModel = require('./models/booking');
const copyModel = require('./models/copy');
const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const memberModel = require('./models/member');
const movieModel = require('./models/movie');
const movieActorModel = require('./models/movieactor');
//poner los demas aqui


// 1) db name 2) user 3) password 4) Obj conf

const sequelize = new Sequelize('video-club', 'root', 'abcd1234',{
  host: 'localhost',
  dialect:'mysql'
});

const Actor = actorModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);

//Un miembro puede agendar varias veces
Member.hasMany(Booking, {as:'bookings'});
//Una reservacion solo puede tener un miembro
Booking.belongsTo(Member, {as:'members'});
//Una copia se puede agendar varias veces
Copy.hasMany(Booking, {as:'bookings2'});
//Una reservacion solo puede tener un miembro
Booking.belongsTo(Copy, {as:'copie'});

//Una pelicula puede tener varias copias
Movie.hasMany(Copy, {as:'copies'});
//Una copia solo puede ser de una pelicula
Copy.belongsTo(Movie, {as:'movie'});

//Un miembro puede agendar varias veces
Member.hasMany(Booking, {as:'bookings3'});
//Una reservacion solo puede tener un miembro
Booking.belongsTo(Member, {as:'members2'});

//Un genero puede tener varias pelis
Genre.hasMany(Movie, {as:'movies'});
//Una peliculasolo puede tener un genero
Movie.belongsTo(Genre, {as:'genre'});
//Un director puede tener muchas peliculas
Director.hasMany(Movie, {as:'movie'})
//Una pelicula puede tener un director
Movie.belongsTo(Director, {as:'director'});
//Un actor participa en muchas pelicula
MovieActor.belongsTo(Movie, {foreignKey:'movieId'});
//En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey:'actorId'});

Movie.belongsToMany(Actor, {
  foreignKey: 'actorId',
  as: 'actors',
  through: 'moviesActors'
});

Actor.belongsToMany(Movie, {
  foreignKey: 'movieId',
  as: 'movies',
  through: 'moviesActors'
});


sequelize.sync({
  force: true
}).then(()=>{
  console.log("Base de datos actualizada correctamente");
});

module.exports = {Director,Genre};
