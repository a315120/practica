module.exports = (sequelize, type) => {
	const Copy = sequelize.define('copy',{
		id: {type: type.INTEGER, primaryKey:true, autoIncrement:true},
		number: type.INTEGER,
		format: type.STRING,
		estatus: type.STRING
	});
  return Copy;
}
