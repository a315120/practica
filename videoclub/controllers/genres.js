const express = require('express');
const { Genre } = require('../db');

const list = (req,res,next) => {
    Genre.findAll()
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Genre.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const create = (req,res,next) => {
    const description = req.body.description;
    const status = req.body.status;

    let genre = new Object({
        description:description,
        status:status
    });

    Genre.create(genre)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Genre.findByPk(id)
            .then((obj) => {
                const description = req.body.description ? req.body.description : "";
                const status = req.body.status ? req.body.status : obj.status;
                object.upadte({description:description,status:status})
                .then(genre => res.json(genre))
            })
            .catch(err => res.send(err));
}

const edit = (req,res,next) => {
const id = req.params.id;
    Genre.findByPk(id)
            .then((obj) => {
                const description = req.body.description ? req.body.description : obj.description;
                const status = req.body.status ? req.body.status : obj.status;
                object.upadte({description:description,status:status})
                .then(genre => res.json(genre))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Genre.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
