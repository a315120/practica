const express = require('express');
const { Member } = require('../db');

const list = (req,res,next) => {
    Member.findAll({include:['bookings']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Member.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const create = (req,res,next) => {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const address = req.body.address;
    const phone = req.body.phone;
    const status = req.body.status;

    let member = new Object({
        name:name,
        lastName:lastName,
        address:address,
        phone:phone,
        status:status
    });

    Member.create(member)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Member.findByPk(id)
            .then((obj) => {
                const name = req.body.name ? req.body.name : "";
                const lastName = req.body.lastName ? req.body.lastName : "";
                const address = req.body.address ? req.body.address : "";
                const phone = req.body.phone ? req.body.phone : "";
                const status = req.body.status ? req.body.status : "";
                object.upadte({
                    name:name,
                    lastName:lastName,
                    address:address,
                    phone:phone,
                    status:status
                })
                .then(member => res.json(member))
            })
            .catch(err => res.send(err));

}

const edit = (req,res,next) => {
    const id = req.params.id;
    Member.findByPk(id)
            .then((obj) => {
                const name = req.body.name ? req.body.name : obj.name;
                const lastName = req.body.lastName ? req.body.lastName : obj.name;
                const address = req.body.address ? req.body.address : obj.address;
                const phone = req.body.phone ? req.body.phone : obj.phone;
                const status = req.body.status ? req.body.status : obj.status;
                object.upadte({
                    name:name,
                    lastName:lastName,
                    address:address,
                    phone:phone,
                    status:status
                })
                .then(member => res.json(member))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Member.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
