const express = require('express');
const { Movie, Actor } = require('../db');

const list = (req,res,next) => {
    Movie.findAll({include:['genre','director','actors']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Movie.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const addActor = (req,res,next) => {
    const idMovie = req.body.idMovie;
    const idActor = req.body.idActor;

    Movie.findByPk(idMovie).then((movie) => {
        Actor.findByPk(idActor).then((actor) => {
            movie.addActor(actor);
            res.json(movie);
        });
    });
}

const create = (req,res,next) => {
    const title = req.body.title;
    const genreId = req.body.genreId;
    const directorId = req.body.directorId;

    let movie = new Object({
        title:title,
        genreId:genreId,
        directorId:directorId
    });

    Movie.create(movie)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Movie.findByPk(id)
            .then((obj) => {
                const title = req.body.title ? req.body.title : "";
                const genreId = req.body.genreId ? req.body.genreId : "";
                const directorId = req.body.directorId ? req.body.directorId : "";
                object.upadte({title:title,genreId:genreId,directorId:directorId})
                .then(movie => res.json(movie))
            })
            .catch(err => res.send(err));

}

const edit = (req,res,next) => {
    const id = req.params.id;
    Movie.findByPk(id)
            .then((obj) => {
                const name = req.body.name ? req.body.name : obj.name;
                const lastName = req.body.lastName ? req.body.lastName : obj.name;
                object.upadte({name:name,lastName:lastName})
                .then(movie => res.json(movie))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Movie.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy,addActor
}
