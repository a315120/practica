const express = require('express');
const { Actor } = require('../db');

const list = (req,res,next) => {
    Actor.findAll({include:['actors','movies']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Actor.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const create = (req,res,next) => {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let actor = new Object({
        name:name,
        lastName:lastName
    });

    Actor.create(actor)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Actor.findByPk(id)
            .then((obj) => {
                const name = req.body.name ? req.body.name : "";
                const lastName = req.body.lastName ? req.body.lastName : "";
                object.upadte({name:name,lastName:lastName})
                .then(actor => res.json(actor))
            })
            .catch(err => res.send(err));

}

const edit = (req,res,next) => {
    const id = req.params.id;
    Actor.findByPk(id)
            .then((obj) => {
                const name = req.body.name ? req.body.name : obj.name;
                const lastName = req.body.lastName ? req.body.lastName : obj.name;
                object.upadte({name:name,lastName:lastName})
                .then(actor => res.json(actor))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Actor.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
