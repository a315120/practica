const express = require('express');

const list = (req,res,next) => {
    res.send('Lista de usuarios del sistema');
}

const index = (req,res,next) => {
    res.send(`Usuario del sistema con un ID: ${req.params.id}`);
}

const create = (req,res,next) => {
    res.send('Crear un usuario nuevo');
}

const replace = (req,res,next) => {
    res.send(`Remplaza un usuario del sistema con un ID: ${req.params.id} por otro.`);
}

const edit = (req,res,next) => {
    res.send(`Remplaza las proiedades de un usuario del sistema con un ID: ${req.params.id} por otras.`);
}

const destroy = (req,res,next) => {
    res.send(`Elimina un usuario del sistema con un ID: ${req.params.id}.`);
}

module.exports = {
    list, index, create, replace, edit, destroy
}
