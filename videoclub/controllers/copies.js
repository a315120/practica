const express = require('express');
const { Copie } = require('../db');

const list = (req,res,next) => {
    Copie.findAll({include:['bookings','movie']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Copie.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const create = (req,res,next) => {
    const number = req.body.number;
    const format = req.body.format;
    const movieId = req.body.movieId;
    const estaus = req.body.estaus;

    let copie = new Object({
        number:number,
        format:format,
        movieId:movieId,
        estaus:estaus
    });

    Copie.create(copie)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Copie.findByPk(id)
            .then((obj) => {
                const number = req.body.number ? req.body.number : "";
                const format = req.body.format ? req.body.format : "";
                const movieId = req.body.movieId ? req.body.movieId : "";
                const estaus = req.body.estaus ? req.body.estaus : "";
                object.upadte({
                    number:number,
                    format:format,
                    movieId:movieId,
                    estaus:estaus
                })
                .then(copie => res.json(copie))
            })
            .catch(err => res.send(err));

}

const edit = (req,res,next) => {
    const id = req.params.id;
    Copie.findByPk(id)
            .then((obj) => {
                const number = req.body.number ? req.body.number : obj.number;
                const format = req.body.format ? req.body.format : obj.format;
                const estaus = req.body.estaus ? req.body.estaus : obj.estaus;
                object.upadte({
                    number:number,
                    format:format,
                    estaus:estaus
                })
                .then(copie => res.json(copie))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Copie.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
