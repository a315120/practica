const express = require('express');

const home = (req,res,next) => {
    res.render('index', {title: 'Express'});
}

module.exports = {
    home
}