const express = require('express');
const { Booking } = require('../db');

const list = (req,res,next) => {
    Booking.findAll({include:['member','copie']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));
}

const index = (req,res,next) => {
    const id = req.params.id;
    Booking.findByPk(id)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const create = (req,res,next) => {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copieId = req.body.copieId;

    let booking = new Object({
        date:date,
        memberId:memberId,
        copieId:copieId
    });

    Booking.create(booking)
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

const replace = (req,res,next) => {
    const id = req.params.id;
    Booking.findByPk(id)
            .then((obj) => {
                const date = req.body.date ? req.body.date : "";
                const memberId = req.body.memberId ? req.body.memberId : "";
                const copieId = req.body.copieId ? req.body.copieId : "";
                object.upadte({date:date,memberId:memberId,copieId:copieId})
                .then(booking => res.json(booking))
            })
            .catch(err => res.send(err));

}

const edit = (req,res,next) => {
    const id = req.params.id;
    Booking.findByPk(id)
            .then((obj) => {
                const date = req.body.date ? req.body.date : obj.date;
                const memberId = req.body.memberId ? req.body.memberId : obj.memberId;
                const copieId = req.body.copieId ? req.body.copieId : obj.copieId;
                object.upadte({date:date,memberId:memberId,copieId:copieId})
                .then(booking => res.json(booking))
            })
            .catch(err => res.send(err));
}

const destroy = (req,res,next) => {
    const id = req.params.id;
    Booking.destroy({where:{id:id}})
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
